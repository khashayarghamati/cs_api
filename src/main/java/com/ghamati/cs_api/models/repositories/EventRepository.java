package com.ghamati.cs_api.models.repositories;

;

import com.ghamati.cs_api.models.EventModel;
import org.springframework.data.repository.CrudRepository;

public interface EventRepository extends CrudRepository<EventModel, Integer> {

    public EventModel findById(Long id);
}
