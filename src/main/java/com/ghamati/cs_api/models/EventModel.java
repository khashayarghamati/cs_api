package com.ghamati.cs_api.models;


import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;

@Entity
public class EventModel {

    public EventModel(
            Long id,
            @NotNull Date beginDate,
            @NotNull Date endDate,
            @NotNull String description,
            @NotNull boolean isHeld
    ) {
        this.id = id;
        this.beginDate = beginDate;
        this.endDate = endDate;
        this.description = description;
        this.isHeld = isHeld;
    }

    public EventModel() {

    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getBeginDate() {
        return beginDate;
    }

    public void setBeginDate(Date beginDate) {
        this.beginDate = beginDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean getisHeld() {
        return isHeld;
    }

    public void setisHeld(boolean held) {
        isHeld = held;
    }

    @Id
    @Column(name = "id", nullable = false)
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "beginDate", nullable = false)
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date beginDate;

    @Column(name = "endDate", nullable = false)
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date endDate;

    @Column(name = "description", nullable = false, length = 100)
    private String description;

    @Column(name = "isHeld", nullable = false)
    private boolean isHeld;
}
