package com.ghamati.cs_api.controllers;

import com.ghamati.cs_api.models.EventModel;
import com.ghamati.cs_api.services.EventService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class EventController {

    @Autowired
    private EventService eventService;

    @RequestMapping(method = RequestMethod.POST, value = "/add")
    public ResponseEntity<Object> addEvent(@RequestBody EventModel eventModel){
        return eventService.addEvent(eventModel);
    }

    @RequestMapping(method = RequestMethod.DELETE, value = "/delete")
    public ResponseEntity<Object> deleteEvent(@RequestBody EventModel eventModel){
        return eventService.deleteEvent(eventModel);
    }

    @RequestMapping(method = RequestMethod.PUT, value = "/update")
    public ResponseEntity<Object> updateEvent(@RequestBody EventModel eventModel){
        return eventService.updateEvent(eventModel);
    }

    @RequestMapping(method = RequestMethod.GET, value = "/events")
    public List<EventModel> getEvents(){
        return eventService.getAllEvent();
    }
}
