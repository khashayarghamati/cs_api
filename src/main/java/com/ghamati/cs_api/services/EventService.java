package com.ghamati.cs_api.services;


import com.ghamati.cs_api.models.repositories.EventRepository;
import com.ghamati.cs_api.models.EventModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import javax.validation.constraints.NotNull;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

@Service
public class EventService {

    public enum DataConditions {
        BeingIsLowerThanCurrent(0),
        EndIsBiggerThanBeing(0);


        private int conditionNumber;

        DataConditions(int conditionNumber) {
            this.conditionNumber = conditionNumber;
        }

        public int conditionNumber() {
            return conditionNumber;
        }
    }

    @Autowired
    private EventRepository eventRepository;


    public ResponseEntity<Object> addEvent(@NotNull EventModel eventModel){

        Date currentDate = this.getCurrentDate();

        if (eventModel
                .getBeginDate()
                .compareTo(currentDate) <
                DataConditions.BeingIsLowerThanCurrent.conditionNumber() ||
                eventModel.getBeginDate().compareTo(eventModel.getEndDate()) >
                        DataConditions.EndIsBiggerThanBeing.conditionNumber()
        )
            return ResponseEntity.badRequest().body(this.getResponseCode(401));

        EventModel model = eventRepository.save(eventModel);

        HashMap<String, Object>response=this.getResponseCode(201);
        response.put("id", model.getId());

        return ResponseEntity.created(null).body(response);
    }


    public ResponseEntity<Object> deleteEvent(@NotNull EventModel eventModel){

        if(eventModel.getId() == null)
            return ResponseEntity.badRequest().body(this.getResponseCode(400));

        if (eventRepository.findById(eventModel.getId()).getisHeld())
            return ResponseEntity.badRequest().body(this.getResponseCode(403));


        eventRepository.delete(eventModel);
        return ResponseEntity.accepted().body(this.getResponseCode(202));
    }


    public ResponseEntity<Object> updateEvent(@NotNull EventModel eventModel){

        if(eventModel.getId() == null)
            return ResponseEntity.badRequest().body(this.getResponseCode(400));

        if (eventRepository.findById(eventModel.getId()).getisHeld())
            return ResponseEntity.badRequest().body(this.getResponseCode(403));

        Date currentDate = this.getCurrentDate();
        if (eventModel
                .getBeginDate()
                .compareTo(currentDate) <
                DataConditions.BeingIsLowerThanCurrent.conditionNumber() ||
                eventModel.getBeginDate().compareTo(eventModel.getEndDate()) >
                        DataConditions.EndIsBiggerThanBeing.conditionNumber()
        )
            return ResponseEntity.badRequest().body(this.getResponseCode(401));

        EventModel model = eventRepository.save(eventModel);

        HashMap<String, Object>response=this.getResponseCode(201);
        response.put("id", model.getId());

        return ResponseEntity.accepted().body(this.getResponseCode(202));
    }


    public List<EventModel> getAllEvent() {
        List<EventModel> models=new ArrayList<>();

        eventRepository.findAll().forEach(models::add);

        return models;
    }


    private HashMap<String, Object> getResponseCode(int code){
        HashMap<String, Object> response = new HashMap<>();
        switch (code){
            case 201: {
                response.put("status", "Your information has been saved");
            }
            break;

            case 202: {
                response.put("status", "Your request accepted");
            }
            break;

            case 401:{
                response.put("status", "We could not save your information because of your beingDate or endDate");
            }
            break;

            case 403:{
                response.put("status", "We could not change your information because it's protected");
            }
            break;

            case 400:{
                response.put("status", "Your request must contain ID of record");
            }
            break;
        }

        return response;
    }


    private Date getCurrentDate(){
        Date currentDate= null;
        try {
            currentDate = new SimpleDateFormat("yyyy-MM-dd").parse(LocalDateTime.now().toString());
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return currentDate;
    }


}
